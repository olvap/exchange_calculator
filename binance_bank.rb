class BinanceBank
  attr_reader :rates

  def initialize(opts = {}, rates = {})
    @rates = rates
  end

  def exchange_with(from, to_currency, &block)
    binding.pry
  end

  # Add new exchange rate.
  # @param [String] iso_from Currency ISO code. ex. 'USD'
  # @param [String] iso_to Currency ISO code. ex. 'CAD'
  # @param [Numeric] rate Exchange rate. ex. 0.0016
  #
  # @return [Numeric] rate.
  def add_rate(iso_from, iso_to, rate)
    puts 'add'
  end

  # Get rate. Must be idempotent. i.e. adding the same rate must not produce duplicates.
  # @param [String] iso_from Currency ISO code. ex. 'USD'
  # @param [String] iso_to Currency ISO code. ex. 'CAD'
  #
  # @return [Numeric] rate.
  def get_rate(currency_iso_from, currency_iso_to)
    puts rates[rate_key_for(currency_iso_from, currency_iso_to)]
    rates[rate_key_for(currency_iso_from, currency_iso_to)]
  end

  # Iterate over rate tuples (iso_from, iso_to, rate)
  #
  # @yieldparam iso_from [String] Currency ISO string.
  # @yieldparam iso_to [String] Currency ISO string.
  # @yieldparam rate [Numeric] Exchange rate.
  #
  # @return [Enumerator]
  #
  # @example
  #   store.each_rate do |iso_from, iso_to, rate|
  #     puts [iso_from, iso_to, rate].join
  #   end
  def each_rate(&block)
    puts '...'
  end

  # Wrap store operations in a thread-safe transaction
  # (or IO or Database transaction, depending on your implementation)
  #
  # @yield [n] Block that will be wrapped in transaction.
  #
  # @example
  #   store.transaction do
  #     store.add_rate('USD', 'CAD', 0.9)
  #     store.add_rate('USD', 'CLP', 0.0016)
  #   end
  def transaction(&block)
    puts '...'
  end

  # Serialize store and its content to make Marshal.dump work.
  #
  # Returns an array with store class and any arguments needed to initialize the store in the current state.

  # @return [Array] [class, arg1, arg2]
  def marshal_dump
    puts '...'
  end
end
