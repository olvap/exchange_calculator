#!/usr/bin/env ruby

require 'httparty'
require 'json'
require 'money'
require 'pry'
require 'monetize'

## data fixer
fixer = HTTParty.get(
 "http://data.fixer.io/api/latest?access_key=bdcce73e58b41b6d585cb529a35ef2f6",
  format: :json
)

Money.rounding_mode = BigDecimal::ROUND_HALF_EVEN
Money.locale_backend = nil

fixer['rates'].each do |currency, rate|
  Money.add_rate('EUR', currency, rate)
  Money.add_rate(currency, 'EUR', 1/rate)
end

# dolar blue
ambito = HTTParty.get(
  'https://mercados.ambito.com//home/general',
  format: :json
)
blue = ambito.select {|dolar| dolar["nombre"] == "Dólar Informal"}.first
blue = (blue["compra"].gsub(',', '.').to_f + blue["venta"].gsub(',', '.').to_f)  / 2

Money.add_rate('USD', 'ARS', blue)
Money.add_rate('ARS', 'USD', 1/blue)

# Resto de las conversiones
usd_to_btc = Money.default_bank.rates["EUR_TO_BTC"] * Money.default_bank.rates["USD_TO_EUR"]
Money.add_rate('USD', 'BTC', usd_to_btc)
Money.add_rate('BTC', 'USD', 1/usd_to_btc)

ars_to_btc = blue/usd_to_btc
Money.add_rate('BTC', 'ARS', ars_to_btc)
Money.add_rate('ARS', 'BTC', 1 / ars_to_btc)


# usage:
# to sell 4000 usd in btc at 3% comision
#
# ("4000 USD".to_money.exchange_to(:btc) * 0.97).format
binding.pry

puts 'bye'
